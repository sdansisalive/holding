#!/bin/bash
set -o errexit

source "/opt/ros/${ROS_DISTRO}/setup.bash"
source "/ros-test/catkin_ws/devel/setup.bash"
exec "$@"
