# Docker Containers for App

## TODOS

* [*] launch files for apps
* [*] ros_entrypoint.sh
* [*] compose file
    * [gliderlabs dns resolver](https://gist.github.com/ruffsl/4a24c26a1aa2cc733c64)
* reevaluate for best practices.

    * [docker build file without shell hack](https://answers.ros.org/answers/312728/revisions/)
    * git clone in build vs pulling/cloning in pipeline.
    * Should I have my dockerfiles and runner scripts in the same repo as code in the case of a pipeline.
        * We chose "no" at cpanel with jenkins. 
            * What was the justification?
            * Counter-arguments?
